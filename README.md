# C++ keylogger for Linux with Xserver

This code illustrates that every X application can listen to the whole keyboard events, even if it does not get the focus.

## Files description

- keylogger.cpp is the source code

- keylogger is a built executable from the source code

keystrokes are dumped into the "keys_log.txt" file


## Rebuild the executable

### Compile
 g++ -ansi -Wall -Wno-deprecated-declarations -pedantic -O3 -o keylogger keylogger.cpp -L/usr/X11R6/lib -lX11
 
### Remove symbols
 strip -s -R .comment -R .note -R .note.ABI-tag keylogger
